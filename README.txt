Pour lancer le fichier RPG.py:

-assurez-vous d'avoir python IDLE pour lancer le fichier (autrement, le fichier ne pourra pas démarrer)
-ouvrez le fichier en faisant un clic droit dessus et en sélectionnant "edit with IDLE"
-une fois le code ouvert, appuyez sur F5 ou appuyez sur "run" en haut de la page, puis "run module  F5" pour éxécuter le script dans la console
-une fois en console, vous n'aurez plus qu'à taper "start()" comme indiqué dans la console pour commencer.

-assurez-vous, lorque vous avez à saisir le nom d'un objet de le saisir avec la bonne orthographe, autrement le programme ne le prendra pas en compte
